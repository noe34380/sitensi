<!DOCTYPE html>
<html>
    <head>
    <title>Orelsan - Tournée</title>
    <link rel="stylesheet" type="text/css" href="src/css/index.css">
    <link rel="stylesheet" type="text/css" href="src/css/tournee.css">
</head>
<body>
    <header>
        <a href="index.html"><img src="images/logo.png"></a>
        <nav>
            <ul>
                <span class="vertical-separator"></span>
                <li><a href="civilisation.html">
                    <span class="nav-text">TOURNÉE</span>
                </a></li>
                <li><a href="civilisation.html">
                    <img src="images/civilisation.jpg">
                    <span class="nav-text">CIVILISATION</span>
                </a></li>
                <li><a href="civilisation.html">
                    <img src="images/la fete est finie.jpg">
                    <span class="nav-text">LA FÊTE EST FINIE</span>
                </a></li>
                <li><a href="civilisation.html">
                    <img src="images/le chant des sirenes.jpg">
                    <span class="nav-text">LE CHANT DES SIRÈNES</span>
                </a></li>
            </ul>
        </nav>
        <div id="user-data">
            maceo.freguis@gmail.com
        </div>
    </header>
    <main>
        <h2>Tournée</h2>
        <h3>Achetez vos places pour la tournée 2023 d'Orelsan.</h3>
        <p>Après la tournée de promotion de son nouvel album <a href="civilisation.html" class="ref-link">Civilisation</a>, Orelsan revient pour la saison des festivals pour ce début d'année 2023, trouvez la date la plus proche de chez vous et réservez votre place en dessous !</p>
        <ul>
            <li>
                <span class="concert-place">Saint-Laurent de Cuves</span>
                <span class="concert-date">27/05/2023</span>
                <span class="concert-name">Festival Papillon de nuit</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
            <li>
                <span class="concert-place">Ruoms</span>
                <span class="concert-date">29/06/2023</span>
                <span class="concert-name">Aluna festival</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
            <li>
                <span class="concert-place">Sermamagny</span>
                <span class="concert-date">30/06/2023</span>
                <span class="concert-name">Les Eurockéennes de Belfort</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
            <li>
                <span class="concert-place">Monts</span>
                <span class="concert-date">07/07/2023</span>
                <span class="concert-name">Festival Terres du son</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
            <li>
                <span class="concert-place">Saint-Nolff</span>
                <span class="concert-date">09/07/2023</span>
                <span class="concert-name">Fête du Bruit</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
            <li>
                <span class="concert-place">St Laurent sur Sèvre</span>
                <span class="concert-date">19/07/2023</span>
                <span class="concert-name">Festival de Poupet</span>
                <div class="date-reservation">
                    <span>Réservez vos places</span>
                    <input type="number" name="place-0" value="0" min="0" max="20">
                </div>
            </li>
        </ul>
        <div id="billing">
            <span>Total :</span>
            <span id="nb-places">0 places</span>
            <span id="price">0.00€</span>
            <span id="pay-button">Payer</span>
        </div>
    </main>
    <div id="checkout-box" class="visible-popup">
        <form action="tournee.php" method="post">
            <h3>Connectez vous pour continuer :</h3>
            <span>
                Email :<br>
                <input type="email" name="email" placeholder="dupont@example.com">
            </span>
            <span>
                Mot de Passe :<br>
                <input type="email" name="email" placeholder="dupont@example.com">
            </span>
            <span>
                <input type="submit" value="Se connecter">
            </span>
        </form>
    </div>
    <script src="src/js/tournee.js"></script>
</body>
</html>