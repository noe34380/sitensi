Object.values(document.getElementsByTagName("input")).forEach(element => {
    element.onchange = updatePrice
});

function updatePrice(event) {
    let inputs = Object.values(document.querySelectorAll("input[type='number']")).map(x => +x.value)
    let places = inputs.reduce((a, b) => a+b)
    document.getElementById("nb-places").innerText = places + " places,"
    document.getElementById("price").innerText = places*45 + "€"
}

window.onkeyup = (event) => {
    if (event.key == "Escape" && document.getElementById("checkout-box").classList.contains('visible-popup')) {
        document.getElementById("checkout-box").classList = ""
    }
};

document.getElementById("pay-button").onclick = () => {
    document.getElementById("checkout-box").classList = ["visible-popup"]
}